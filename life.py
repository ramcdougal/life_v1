from matplotlib import pyplot
import random

pyplot.interactive(True)

init_x0 = -3
init_x1 = 3
init_y0 = -3
init_y1 = 3

def plot_it(items):
    x = [item[0] for item in items]
    y = [item[1] for item in items]
    pyplot.scatter(x, y)

def init():
    pts = []
    for x in xrange(init_x0, init_x1 + 1):
        for y in xrange(init_y0, init_y1 + 1):
            if random.random() < .5:
                pts.append((x, y))
    return pts

def get_neighbor_count(x, y):
    neighbor_count = 0
    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            if i != 0 or j !=0:
                if (i + x, j + y) in items:
                    neighbor_count += 1
    return neighbor_count
    
def advance(items):
    new_items = set([])
    for x, y in items:
        neighbor_count = get_neighbor_count(x, y)
        if neighbor_count < 2:
            pass
        elif 1 < neighbor_count < 4:
            new_items.add((x, y))
        elif neighbor_count > 3:
            pass
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                neighbor_count = get_neighbor_count(x + i, y + j)
                if neighbor_count == 3:
                    new_items.add((x + i, y + j))
    new_items = set(new_items)
    return new_items
    
items = init()

plot_it(items)
t = 0
pyplot.title('t = %g' % t)
while True:
    t += 1
    raw_input()
    items = advance(items)
    pyplot.clf()
    plot_it(items)
    pyplot.title('t = %g' % t)

pyplot.show()
